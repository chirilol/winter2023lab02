import java.util.Scanner;
public class PartThree{

	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		System.out.println("First number: ");	
		int firstUserInput = keyboard.nextInt();
		System.out.println("Second number: ");	
		int secondUserInput = keyboard.nextInt();
		//Scanner Var Assignment
		
		System.out.println("Add: "+ Calculator.add(firstUserInput,secondUserInput));
		System.out.println("Substract: "+ Calculator.substract(firstUserInput,secondUserInput));
		
		Calculator cal = new Calculator(); //for instance methods -multiply and divide-
		
		System.out.println("Multiply: "+ cal.multiply(firstUserInput,secondUserInput));
		System.out.println("Divide: "+ cal.divide(firstUserInput,secondUserInput));
		
	}

}