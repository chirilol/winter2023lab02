public class MethodsTest{
	
	public static void methodNoInputNoReturn(){
			System.out.println("I’m in a method that takes no input and returns nothing");
			int x = 20;
			System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int aNumber){
		System.out.println("Inside the method one input no return");
		aNumber -= 5;
		System.out.println(aNumber);
	}
	
	public static void methodTwoInputNoReturn(int firstNumber, double secNumber){
		System.out.println(firstNumber);
		System.out.println(secNumber);
	}
	
	public static int methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int numberOne, int numberTwo){
		int sumOfNumbers = numberOne+numberTwo;
		return Math.sqrt(sumOfNumbers);
	}
	
	public static void main(String[] args){
			int x = 5;
			System.out.println(x);
			methodNoInputNoReturn();
			System.out.println(x);
			methodOneInputNoReturn(x+10);
			System.out.println(x);
			methodTwoInputNoReturn(3,7);
			int z = methodNoInputReturnInt();
			System.out.println(z);
			double squareResult = sumSquareRoot(9,5);
			System.out.println(squareResult);
			String s1 = "java";
			String s2 = "programming";
			System.out.println(s1.length());
			System.out.println(s2.length());
			System.out.println(SecondClass.addOne(50));
			SecondClass sc = new SecondClass();
			System.out.println(sc.addTwo(50));
	}
	
}